(function($) {
    //window.ee_challenge is a global object used when interacting with the API.
    window.ee_challenge = {
        apiURL: 'http://localhost:3000/',
        addDSU: function(name, description, cert) {
            console.log('Adding DSU');
            //returning a promise that is handled in the add-dsu template
            return $.ajax({
                type: "PUT",
                url: this.apiURL + 'v1/api/dsus',
                data: { name: name, description: description, cert: cert },
            });
        },
        addSite: function(dsuID, name, description) {
            console.log('Adding site');
            //returning a promise that is handled in the add-site template
            return $.ajax({
                type: "PUT",
                url: this.apiURL + 'v1/api/sites',
                data: { dsuId: parseInt(dsuID), name: name, description: description }
            });
        },
        fetchDSUs: function() {
            console.log('Fetching DSUs');
            //returning a promise that is handled in the router
            return this.fetch(this.apiURL + 'v1/api/dsus');
        },
        fetchSites: function() {
            console.log('Fetching Sites');
            //returning a promise that is handled in the router
            return this.fetch(this.apiURL + 'v1/api/sites');
        },
        fetch: function(url) {
            //generic fetch
            console.log('Fetching ' + url);
            return $.ajax({
                context: this,
                url: url,
                data: {},
                method: "GET",
                dataType: "JSON"
            });
        }
    };
})(jQuery)


// Utility (helper) functions
var utils = {

    //retrieves a page template, compiles it and passes data into the compiled template.
    renderPageTemplate: function(templateId, data) {
        $.get(templateId, function(d) {
            var template = Handlebars.compile(d);
            $("#page-container").empty().append(template(data));
        }, 'html');
    },

    // If a hash can not be found in routes
    // then this function gets called to show the 404 error page
    pageNotFoundError: function() {
        var data = {
            errorMessage: "404 - Page Not Found"
        };
        this.renderPageTemplate("templates/error.hbs", data);
    },
};


/**
 *  Router - Handles routing and rendering for the order pages
 *
 *  Summary:
 *      - url hash changes
 *      - render function checks routes for the hash changes
 *      - function for that hash gets called and loads page content 
 */
var router = {

    // An object of all the routes
    routes: {},
    init: function() {
        console.log('router was created...');
        this.bindEvents();

        // Manually trigger a hashchange to start the router.
        // This make the render function look for the route called "" (empty string)
        // and call it"s function
        $(window).trigger("hashchange");
    },
    bindEvents: function() {

        // Event handler that calls the render function on every hashchange.
        // The render function will look up the route and call the function
        // that is mapped to the route name in the route map.
        // .bind(this) changes the scope of the function to the
        // current object rather than the element the event is bound to.
        $(window).on("hashchange", this.render.bind(this));
    },
    // Checks the current url hash tag
    // and calls the function with that name
    // in the routes
    render: function() {
        $('form').dirtyForms('rescan');
        if( $('form').dirtyForms('isDirty') ){
            if(!confirm("Are you sure you want to leave? Your changes will not be saved.")){
                return false;
            }
        }


        // Get the keyword from the url.
        var keyName = window.location.hash.split("/")[0];

        // Grab anything after the hash

        // Call the the function
        // by key name
        if (this.routes[keyName]) {
            this.routes[keyName]();

            // Render the error page if the 
            // keyword is not found in routes.
        } else {
            utils.pageNotFoundError();
        }
    }
};

var spaRoutes = {
    "": function() {
        //render homepage template
        utils.renderPageTemplate("templates/home.hbs");
    },
    "#home": function() {
        //render homepage template
        utils.renderPageTemplate("templates/home.hbs");
    },
    "#dsu-list": function() {
        //fetch data from the API, when the data is returned render the template
        $.when( ee_challenge.fetchDSUs() ).done( function(data) {
            utils.renderPageTemplate("templates/dsu-list.hbs", data);
        } );
    },
    "#site-list": function() {
        //fetch data from the API, when the data is returned render the template
        $.when( ee_challenge.fetchSites() ).done( function(data) {
            utils.renderPageTemplate("templates/site-list.hbs", data);
        } );
    },
    "#add-dsu": function() {
        //render add DSU page
        utils.renderPageTemplate("templates/add-dsu.hbs");
    },
    "#add-site": function() {
        //retrieve list of DSUs then render add site page.
        $.when(ee_challenge.fetchDSUs()).done(function(data) {
            utils.renderPageTemplate("templates/add-site.hbs", data);
        });
    },
};

// Create a new instance of the router
var spaRouter = $.extend({}, router, {
    routes: spaRoutes
});

spaRouter.init();