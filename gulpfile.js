var gulp = require('gulp');
var sass = require('gulp-sass');
var serve = require('gulp-serve');

gulp.task('styles', function(){
  return gulp.src('src/scss/main.scss')
    .pipe(sass()) // Converts Sass to CSS with gulp-sass
    .pipe(gulp.dest('dist/css'))
});

gulp.task('serve', serve({
  root: ['./'],
  port: 8888
}));

gulp.task('default', ['styles']);

